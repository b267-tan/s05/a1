<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Activity for session 05</title>
    </head>
    <body>
        
        <?php session_start(); ?>

        <?php if(!isset($_SESSION['user'])): ?>
        	<h3>Login</h3>
            <form method = "POST" action = "./server.php" >
            <input type="hidden" name="action" value="login">
            Email: <input type="email" name="email" required>
            Password: <input type="password" name="password" required>
            <button type="submit">Login</button>
        </form>

        <?php endif; ?>
        

        <?php if(isset($_SESSION['user'])): ?>
            <p>Hello <?php echo $_SESSION['user'] ?> </p>
            <form action="./server.php" method="POST">
                <input type="hidden" name="action" value="logout">
                <button type="submit">Logout</button>
            </form>

        <?php endif; ?>

    </body>
</html>
